package tn.wna.converter;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

/**
 * Read from xlsx and harded xml generation 
 * 
 * @author OXFORT 
 *
 */
public class App 
{   public static final String SAMPLE_XLSX_FILE_PATH = "C:\\Worck\\Demo\\converter\\src\\main\\java\\tn\\wna\\converter/sample-xlsx-file.xlsx";

     public static void main( String[] args ) throws IOException,InvalidFormatException,JAXBException
    {  List<Validator> validators;
    validators=new List<Validator>() {
		
		public <T> T[] toArray(T[] a) {
			// TODO Auto-generated method stub ok
			return null;
		}
		
		public Object[] toArray() {
			// TODO Auto-generated method stub
			return null;
		}
		
		public List<Validator> subList(int fromIndex, int toIndex) {
			// TODO Auto-generated method stub
			return null;
		}
		
		public int size() {
			// TODO Auto-generated method stub
			return 0;
		}
		
		public Validator set(int index, Validator element) {
			// TODO Auto-generated method stub
			return null;
		}
		
		public boolean retainAll(Collection<?> c) {
			// TODO Auto-generated method stub
			return false;
		}
		
		public boolean removeAll(Collection<?> c) {
			// TODO Auto-generated method stub
			return false;
		}
		
		public Validator remove(int index) {
			// TODO Auto-generated method stub
			return null;
		}
		
		public boolean remove(Object o) {
			// TODO Auto-generated method stub
			return false;
		}
		
		public ListIterator<Validator> listIterator(int index) {
			// TODO Auto-generated method stub
			return null;
		}
		
		public ListIterator<Validator> listIterator() {
			// TODO Auto-generated method stub
			return null;
		}
		
		public int lastIndexOf(Object o) {
			// TODO Auto-generated method stub
			return 0;
		}
		
		public Iterator<Validator> iterator() {
			// TODO Auto-generated method stub
			return null;
		}
		
		public boolean isEmpty() {
			// TODO Auto-generated method stub
			return false;
		}
		
		public int indexOf(Object o) {
			// TODO Auto-generated method stub
			return 0;
		}
		
		public Validator get(int index) {
			// TODO Auto-generated method stub
			return null;
		}
		
		public boolean containsAll(Collection<?> c) {
			// TODO Auto-generated method stub
			return false;
		}
		
		public boolean contains(Object o) {
			// TODO Auto-generated method stub
			return false;
		}
		
		public void clear() {
			// TODO Auto-generated method stub
			
		}
		
		public boolean addAll(int index, Collection<? extends Validator> c) {
			// TODO Auto-generated method stub
			return false;
		}
		
		public boolean addAll(Collection<? extends Validator> c) {
			// TODO Auto-generated method stub
			return false;
		}
		
		public void add(int index, Validator element) {
			// TODO Auto-generated method stub
			
		}
		
		public boolean add(Validator e) {
			// TODO Auto-generated method stub
			return false;
		}
	};
        Validator dateValidtor=new LogicValidator();
        dateValidtor.setId(new Long(1));
        dateValidtor.setExecutionAction("change,Load,parse");
        dateValidtor.setLabel("dateValidator");
        System.out.println(dateValidtor.generateValidator());
        validators.add(dateValidtor);
        /**
         * this Class Validator
         */
        CValidator AgeValidtor=new CValidator();
        AgeValidtor.setId(new Long(2));
        AgeValidtor.setExecutionAction("change,Load,parse");
        AgeValidtor.setLabel("dateValidator");
        AgeValidtor.setClassVALIDATOR("src.wna.converter.valid");
        System.out.println(AgeValidtor.generateValidator());
        validators.add(AgeValidtor);
        /**
         * Using Jaxb to converte Pojo to XML 
         */
        
        JAXBContext jaxbContext = JAXBContext.newInstance(Validator.class);
        Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(AgeValidtor, new File("Validator.xml"));
        marshaller.marshal(AgeValidtor, System.out);
        
        /**
         * Read an write in xlsx file 
         */
//        Properties p =new Properties();
//        String basepath = p.getProperty("basePath");
//        System.out.println(basepath);
//        System.out.println(SAMPLE_XLSX_FILE_PATH);
//        System.out.println(Paths.get(SAMPLE_XLSX_FILE_PATH));
        
        // Creating a Workbook from an Excel file (.xls or .xlsx)
        Workbook workbook = WorkbookFactory.create(new File(SAMPLE_XLSX_FILE_PATH));
        // Retrieving the number of sheets in the Workbook
        System.out.println("Workbook has " + workbook.getNumberOfSheets() + " Sheets : ");
        /*
        =============================================================
        Iterating over all the sheets in the workbook (Multiple ways)
        =============================================================
     */

     // 1. You can obtain a sheetIterator and iterate over it
     Iterator<Sheet> sheetIterator = workbook.sheetIterator();
     System.out.println("Retrieving Sheets using Iterator");
     while (sheetIterator.hasNext()) {
         Sheet sheet = sheetIterator.next();
         System.out.println("=> " + sheet.getSheetName());
     }

     // 2. Or you can use a for-each loop
     System.out.println("Retrieving Sheets using for-each loop");
     for(Sheet sheet: workbook) {
         System.out.println("=> " + sheet.getSheetName());
     }

     // 3. Or you can use a Java 8 forEach with lambda
//     System.out.println("Retrieving Sheets using Java 8 forEach with lambda");
//     workbook.forEach(sheet -> {
//         System.out.println("=> " + sheet.getSheetName());
//     });

     /*
        ==================================================================
        Iterating over all the rows and columns in a Sheet (Multiple ways)
        ==================================================================
     */

     // Getting the Sheet at index zero
     Sheet sheet = workbook.getSheetAt(0);

     // Create a DataFormatter to format and get each cell's value as String
     DataFormatter dataFormatter = new DataFormatter();

     // 1. You can obtain a rowIterator and columnIterator and iterate over them
     System.out.println("\n\nIterating over Rows and Columns using Iterator\n");
     Iterator<Row> rowIterator = sheet.rowIterator();
     while (rowIterator.hasNext()) {
         Row row = rowIterator.next();
         System.out.print(row.toString());
         // Now let's iterate over the columns of the current row
         Iterator<Cell> cellIterator = row.cellIterator();

         while (cellIterator.hasNext()) {
             Cell cell = cellIterator.next();
             String cellValue = dataFormatter.formatCellValue(cell);
             System.out.print(cellValue + "\t");
         }
         System.out.println();
     }

     // 2. Or you can use a for-each loop to iterate over the rows and columns
     System.out.println("\n\nIterating over Rows and Columns using for-each loop\n");
     for (Row row: sheet) {
         for(Cell cell: row) {
             String cellValue = dataFormatter.formatCellValue(cell);
             System.out.print(cellValue + "\t");
         }
         System.out.println();
     }

     // 3. Or you can use Java 8 forEach loop with lambda
//     System.out.println("\n\nIterating over Rows and Columns using Java 8 forEach with lambda\n");
//     sheet.forEach(row -> {
//         row.forEach(cell -> {
//             String cellValue = dataFormatter.formatCellValue(cell);
//             System.out.print(cellValue + "\t");
//         });
//         System.out.println();
//     });

     // Closing the workbook
     workbook.close();
 }
        
        
    
}
