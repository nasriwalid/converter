package tn.wna.converter;

import javax.xml.bind.annotation.XmlAnyAttribute;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "validators")
public abstract class Validator {
	@XmlElement(name = "Id")
	private Long id;
	@XmlElement(name = "Name")
	private String label;
	@XmlElement(name = "ExecutionAction")
	private String executionAction;

	public Validator() {
		super();
	}

	public Validator(Long id, String label, String executionAction) {
		super();
		this.id = id;
		this.label = label;
		this.executionAction = executionAction;
	}

//	public Long getId() {
//		return id;
//	}

	public void setId(Long id) {
		this.id = id;
	}

//	public String getLabel() {
//		return label;
//	}
//
	public void setLabel(String label) {
		this.label = label;
	}
//
//	public String getExecutionAction() {
//		return executionAction;
//	}

	public void setExecutionAction(String executionAction) {
		this.executionAction = executionAction;
	}

	public abstract String genrateName();

	public String generateExecutionAction(String value) {
		this.executionAction = value;
		String output = "<executionAction>" + value + "</executionAction>";
		return output;
	}

	public String generateLabel(String value) {
		this.label = value;
		String output = "<Label>" + value + "</Label>";
		return output;
	}

	public String generateExecutionAction() {
		String output = "<executionAction>" + this.executionAction + "</executionAction>";
		return output;
	}

	public String generateLabel() {
		String output = "<Label>" + this.label + "</Label>";
		return output;
	}

	public String generateValidator() {
		String newLine = System.getProperty("line.separator");
		String output = "<valide>" + newLine + this.generateLabel() + newLine + this.genrateName() + newLine
				+ this.generateExecutionAction() + newLine + "</valide>";
		return output;
	}

}
